<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
</head>
<body>
<form action="newMessage"<%--topに遷移したいけど、サーブレットで指定しないと？ --%> method="post">
<label for ="category">カテゴリー(10文字以下)</label>
 <input type ="text" name="category" size="40" maxlength="10"></input><br/>
<label for="title">件名(30文字以下)</label>
                <input type ="text" name="title" size="40" maxlength="30"></input>
                <br />
 <label for="body">新規投稿(1000文字以下)</label>
                <textarea name="body" cols="35" rows="5" id="body" maxlength="1000"></textarea>
                <br /> <input type="submit" value="投稿" /> <br /> <a href="top.jsp">戻る</a>
                </form>
</body>
</html>