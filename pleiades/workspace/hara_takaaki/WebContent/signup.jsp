<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post"><br />
                <label for="name">名前</label> <input name="name" id="name" /><br />
                <label for="login_id">ログインid</label>
                <input name="login_id"id="login_id" /> <br />
                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <select name ="支店名"><option value ="1" >天神支店
                <option value ="2" >博多支店</option>
                <option value ="3" >大宰府支店</option>
                <option value ="4" >小倉支店</option>
                <option value ="5" >久留米支店</option></select><br/>

<%--       <label for="stop_ornot">停止中か否か数字にする？１が停止０が普通とか　通常０にして、入力はなしにする？</label> <input name="stop_ornot" id="stop_ornot" /> <br />--%>

                <select name ="部署・役職">
                <option value ="1" >総務部</option>
                <option value ="2" >人事部</option>
                <option value ="3" >商品部</option>
                <option value ="4" >営業部</option>
                <option value ="5" >物流部</option></select>
                <br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)hara takaaki</div>
        </div>
    </body>
</html>