package jp.alhinc.hara.takaaki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> branchdef = new LinkedHashMap<String, String>();
		Map<String, Long> salefile = new LinkedHashMap<String, Long>();
		BufferedReader br = null;
		BufferedReader br2 = null;

		try {
			File file = new File(args[0], "branch.lst");

			if (!file.exists()) {
				System.out.print("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] str = line.split(",");
				if (str.length != 2 || !str[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchdef.put(str[0], str[1]);
				salefile.put(str[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		List<String> shopidcheck = new ArrayList<String>(branchdef.keySet());
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if (str.matches("[0-9]{8}\\.rcd")) {
					return true;
				} else {
					return false;
				}
			}
		};
		try {
			File[] list = new File(args[0]).listFiles(filter);
			List<Integer> number = new ArrayList<Integer>();
			for (int l = 0; l < list.length; l++) {
				String fileName = list[l].getName();
				number.add(Integer.parseInt(fileName.substring(0, 8)));
			}
			Collections.sort(number);
			for (int k = 1; k < number.size(); k++) {
				if (number.get(k) != number.get(k - 1) + 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			for (int i = 0; i < list.length; i++) {
				try {
					FileReader fr2 = new FileReader(list[i]);
					br2 = new BufferedReader(fr2);
					String branchcode;
					String sales;
					String error;
					branchcode = br2.readLine();
					sales = br2.readLine();
					error = br2.readLine();
					if (error != null || branchcode == null || sales == null) {
						System.out.println(list[i].getName() + "のフォーマットが不正です");
						return;
					}
					if (!(shopidcheck.contains(branchcode))) {
						System.out.println(list[i].getName() + "の支店コードが不正です");
						return;
					}
					long sale = Long.parseLong(sales);
					if (salefile.containsKey(branchcode)) {
						long j = salefile.get(branchcode);
						j += sale;
						if (j > 9999999999L) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
						salefile.put(branchcode, j);
					}
				} finally {
					br2.close();
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		try {
			FileWriter total = new FileWriter(new File(args[0], "branch.out"));
			PrintWriter pw = new PrintWriter(new BufferedWriter(total));
			for (Entry<String, String> entry : branchdef.entrySet()) {
				pw.println(entry.getKey() + "," + entry.getValue() + "," + salefile.get(entry.getKey()));
			}
			pw.close();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}